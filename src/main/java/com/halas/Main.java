package com.halas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger log = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        log.trace("Start main program");
        Flower rose=new Flower("Rose");
        if(!rose.getName().equals("Rose")) {
            log.fatal("I like only roses!");
            System.exit(0);
        }else if(rose.getName().equals("")){
            log.error("Flower can't have empty name");
        }else if(rose.getName().equals("Yurii")){
            log.warn("No no no, it can be, but not so good");
        }
        log.debug("After constructor");
        System.out.println(rose.getName());
        rose.setName("Tulip");
        log.info(rose);

    }
}
