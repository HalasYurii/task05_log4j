package com.halas;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Flower {
    private String name;
    private static Logger log = LogManager.getLogger(Main.class);

    public Flower(String name) {
        log.trace("Flower constructor");
        log.warn("Be careful!");
        this.name = name;
    }

    public String getName() {
        log.debug("function getName()");
        log.error("Hmm wrong action!");
        return name;
    }

    public void setName(String name) {
        log.info("name: "+name);
        log.fatal("stand up and fix it immediately!");
        this.name = name;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                '}';
    }
}
